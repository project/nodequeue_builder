<?php


/* Node Queue Builder - a Drupal module that allows privileged users to add
 * selected nodes to a node queue.
 *
 * Copyright (C) 2007 Thomas Barregren.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/**
 * The name of the default view provided by this module.   
 */
define('NODEQUEUE_BUILDER_DEFAULT_VIEW', 'nodequeue_builder');

/**
 * The default number of nodes per page.   
 */
define('NODEQUEUE_BUILDER_DEFAULT_LIMIT', 10);

/**
 * The path to the settings page.   
 */
define('NODEQUEUE_BUILDER_PATH_SETTINGS', 'admin/content/nodequeue/builder/settings');

/**
 * The path to the builder page.    
 */
define('NODEQUEUE_BUILDER_PATH_BUILDER', 'admin/content/nodequeue/builder');

/**
 * The permission for administrating Node Queue Builder.    
 */
define('NODEQUEUE_BUILDER_PERM_ADMINISTER', 'administer nodequeue_builder');

/**
 * Implementation of hook_help().
 *
 * Returns various help texts.
 */
function nodequeue_builder_help($section='admin/help#nodequeue_builder') {
  switch ($section) {
    case NODEQUEUE_BUILDER_PATH_SETTINGS:
      return _nodequeue_builder_help_admin();
    case NODEQUEUE_BUILDER_PATH_BUILDER:
      return _nodequeue_builder_help_builder();
          case 'admin/help#nodequeue_builder':
      return _nodequeue_builder_help_help();
  }
}

/**
 * Implementation of hook_perm().
 */
function nodequeue_builder_perm()
{
  return array(NODEQUEUE_BUILDER_PERM_ADMINISTER);
}

/**
 * Implementation of hook_menu().
 */
function nodequeue_builder_menu($may_cache) {
  $items = array();
  if ($may_cache) {

    // Node queue Builder administration 
    $items[] = array(
      'path' => NODEQUEUE_BUILDER_PATH_SETTINGS,
      'access' => user_access(NODEQUEUE_BUILDER_PERM_ADMINISTER),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('nodequeue_builder_admin'),
      'title' => t('Settings'),
      'description' => t('Settings for Node Queue Builder'),
      'type' => MENU_LOCAL_TASK
    );
    
  }
  else {
    
    // Node Queue Builder itself 
    $items[] = array(
      'path' => NODEQUEUE_BUILDER_PATH_BUILDER,
      'access' => user_access('administer nodequeue'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('nodequeue_builder_builder'),
      'title' => t('Build'),
      'description' => t('Select and add nodes to a node queue'),
      'type' => MENU_LOCAL_TASK
    );

  }
  return $items;
}

/**
 * Returns the settoings form.
 */
function nodequeue_builder_admin()
{
  
  // Add a selector with possible views that can be used as node list by the builder. 
  $form['nodequeue_builder_view'] = array(
    '#type' => 'select',
    '#title' => 'Builder\'s view',
    '#description' => 'Select the ' . l('view' , 'admin/build/views') . ' that will provide the builder\'s node list.',
    '#options' => _nodequeue_builder_list_views(),
    '#default_value' => variable_get('nodequeue_builder_view', NODEQUEUE_BUILDER_DEFAULT_VIEW),
    '#multiple' => false,
    '#required' => true
  );
  
  // Add a selector with possible number of nodes to show on each page 
  $form['nodequeue_builder_limit'] = array(
    '#type' => 'select',
    '#title' => 'Nodes per builder page',
    '#description' => 'The number of nodes to display per page in the builder.',
    '#options' => array(10 => 10, 20 => 20, 50 => 50, 100 => 100, 0 => 'All nodes'),
    '#default_value' => variable_get('nodequeue_builder_limit', NODEQUEUE_BUILDER_DEFAULT_LIMIT),
    '#multiple' => false,
    '#required' => true
  );
  
  // Return system settings form.
  return system_settings_form($form);

}

/**
 * Returns the builder form.
 */
function nodequeue_builder_builder()
{

  // Load the view that will provide the node list.
  $view_name = variable_get('nodequeue_builder_view', NODEQUEUE_BUILDER_DEFAULT_VIEW);
  $view = views_get_view($view_name);  

  // Handle the case that the view has been removed.
  if(!isset($view)) {
    
    // Set an error message
		$message = t('The view "!view" is no longer avaiable.', array('!view' => $view_name));
		watchdog('nodequeue_builder', $message, WATCHDOG_ERROR);
		drupal_set_message($message, 'error');
    
    // Present an explanation
    $form['message'] = array(
      '#value' => t(
          '<p>Node Queue Builder uses the view "!view" as its source of nodes. But it is no longer avaiable.</p>
  				 <p>Please, <a href="!views-url">add "!view"</a> or <a href="!settings-url">select another view</a>.</p>',
        array(
				  '!view' => $view_name,
				  '!views-url' => url('http://drupal/admin/build/views/add'),
				  '!settings-url' => url(NODEQUEUE_BUILDER_PATH_SETTINGS))
				)
    );
    
    // Nothing more to do.
    return $form;
    
  }

  // Get the number of nodes to be shown on each page.
  $limit = $view_name = variable_get('nodequeue_builder_limit', NODEQUEUE_BUILDER_DEFAULT_LIMIT);
  
  // Let views module make the query.
  $view = views_build_view('result', $view, array(), $limit > 0, $limit);
  $result = $view['result'];
  
  // Form for selecting nodes to be added.
  while ($node = db_fetch_object($result)) {
    $node = node_load($node->nid);
    $nodes[$node->nid] = '';
    $form['title'][$node->nid] = array('#value' => l($node->title, 'node/'. $node->nid) .' '. theme('mark', node_mark($node->nid, $node->changed)));
    $form['name'][$node->nid] =  array('#value' => node_get_types('name', $node));
    $form['created'][$node->nid] = array('#value' => format_date($node->created, 'small'));
    $form['username'][$node->nid] = array('#value' => theme('username', $node));
  }
  $form['nid'] = array('#type' => 'checkboxes', '#options' => $nodes);
  
  // Pager
  $form['pager'] = array('#value' => theme('pager', NULL, $limit, 0));

  // Form for selecting a queue to be updated.
  $form['add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add to node queue'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>'
  );
  $form['add']['qid'] = array('#type' => 'select', '#options' => _nodequeue_builder_list_queues());
  $form['add']['submit'] = array('#type' => 'submit', '#value' => t('Update'));
  
  // Return the form.
  return $form;

}

/**
 * Theme the table of avaiable nodes to build a queue from. 
 */
function theme_nodequeue_builder_builder($form) {
  
  // Render the queue selection and update button
  $output .= drupal_render($form['add']);
  
  // Render the table with the nodes to select from
  $header = array(theme('table_select_header_cell'), t('Title'), t('Type'), t('Created'), t('Author'));
  if (isset($form['title']) && is_array($form['title'])) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      $row[] = drupal_render($form['nid'][$key]);
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['created'][$key]);
      $row[] = drupal_render($form['username'][$key]);
      $rows[] = $row;
    }
  }
  else  {
    $rows[] = array(array('data' => t('No nodes available.'), 'colspan' => '6'));
  }
  $output .= theme('table', $header, $rows);
  
  // Render the pager
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  
  $output .= drupal_render($form);

  // Return the HTML
  return $output;
  
}

function nodequeue_builder_builder_submit($form_id, $form_values) {
  $qid =  $form_values['qid'];
  foreach ($form_values['nid'] as $nid)
    nodequeue_queue_add($qid, $nid);
  return "admin/content/nodequeue/view/$qid";
}

/**
 * Implementation of hook_views_default_views().
 */
function nodequeue_builder_views_default_views()
{
  $view = new stdClass();
  $view->name = NODEQUEUE_BUILDER_DEFAULT_VIEW;
  $view->description = 'Nodes avaiable for Node Queue Builder';
  $view->access = array ();
  $view->view_args_php = '';
  $view->sort = array(
    array(
      'tablename' => 'node',
      'field' => 'sticky',
      'sortorder' => 'DESC',
      'options' => ''
    ),
    array(
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => 'normal'
    )
  );
  $view->argument = array();
  $view->field = array();
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (0 => 'story')
    ),
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1'
    ),
    array (
      'tablename' => 'node',
      'field' => 'distinct',
      'operator' => '=',
      'options' => '',
      'value' => array ()
    )
  );
  $view->exposed_filter = array();
  $view->requires = array(node);
  $views[$view->name] = $view;
  return $views;
}


/* PRIVATE DECLARATIONS *******************************************************
 * 
 * Following declarations are private to this module. They can change and be
 * removed at any time. Don't depend on them!
 *****************************************************************************/

/**
 * Returns an array of avaiable views.
 *  */ 
function _nodequeue_builder_list_views()
{
  $views = array(NODEQUEUE_BUILDER_DEFAULT_VIEW => NODEQUEUE_BUILDER_DEFAULT_VIEW);
  $result = db_query("SELECT name FROM {view_view} ORDER BY name");
  while ($view = db_fetch_object($result)) {
    $views[$view->name] = $view->name;
  }
  return $views;
}

/**
 * Returns an array of avaiable queues.
 */ 
function _nodequeue_builder_list_queues()
{
  $queues = nodequeue_handler_queuelist();
  return isset($queues) ? $queues : array();
}

/**
 * Returns short help text about the settings page.
 */
function _nodequeue_builder_help_admin() {
  return t('
<p>Settings for the Node Queue Builder. For more information, see the !help.</p>
	  ',
    array('!help' => l(t('help page'), 'admin/help/nodequeue_builder'))
  );
}

/**
 * Returns short help text about the builder page.
 */
function _nodequeue_builder_help_builder() {  
  return t('
<p>Add checked nodes to the selected queue. For more information, see the !help.</p>
		',
    array('!help' => l(t('help page'), 'admin/help/nodequeue_builder'))
  );
}
      
/**
 * Returns full help text.
 */
function _nodequeue_builder_help_help() {
  $version = str_replace(array('$Re'.'vision:', ' $'), array('', ''), '$Revision$');
  $year = substr('$Date$', 7, 4);
  return '
<!-- Copyright (C) ' . $year . ' Thomas Barregren <mailto:thomas@webbredaktoren.se> -->
<style type="text/css" media="all">
  code,
  kbd
  {
    padding: 1px;
    font-family: "Bitstream Vera Sans Mono", Monaco, "Lucida Console", monospace;
    background-color: #EDF1F3;
  }
</style>
' . t( '
<p>TODO</p>
<!--break-->
<h2>Background</h2>
<p>TODO</p>
<h2>Install</h2>
<ol>
  <li>TODO</li>
</ol>
<h2>Configuration</h2>
<ol>
  <li>TODO</li>
</ol>
<h2>Usage</h2>
<ol>
  <li>TODO</li>
</ol>
<h2>License</h2>
<p>Node Queue Builder revision !version. Copyright &copy; !year <a href="mailto:thomas@webbredaktoren.se">Thomas Barregren</a>.</p>
<p>Node Queue Builder is free software; you can redistribute it and/or modify it under the terms of the <a href="http://www.gnu.org/licenses/gpl.html#SEC1">GNU General Public License</a> as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</p>
<p>Node Queue Builder is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the <a href="http://www.gnu.org/licenses/gpl.html#SEC1">GNU General Public License</a> for more details.</p>
<p>You should have received a copy of the <a href="http://www.gnu.org/licenses/gpl.html#SEC1">GNU General Public License</a> along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</p>
',
    array('!version' => $version, '!year' => $year)
  );
}
